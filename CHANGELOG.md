<a name="1.1.2"></a>
## 1.1.2 (2021-01-09)


#### Bug Fixes

* **descr:**  remove description ([a76259d8](https://gitlab.com/Luc-AUCOIN/gitlab-to-github--script/commit/a76259d846ab16da434f5d5d5382aa823af62fd1))



<a name="1.1.1"></a>
## 1.1.1 (2021-01-09)


#### Bug Fixes

* **post:**  remove space ([22eb80c9](https://gitlab.com/Luc-AUCOIN/gitlab-to-github--script/commit/22eb80c98121295a10ed3437e16c1ef5a249ab98))



## 1.0.0

Init :
 - Create GitHub repository
 - Push GitLab repository history to GitHub
 - Create a mirror (push) on GitLab repository