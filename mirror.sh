#!/usr/bin/env bash

# Check for project update with a git pull.
echo Checking for updates
if [ -d ".git" ]
then
    (cd $(dirname $0) && git pull)
else
    echo "This is not a git repository, please clone the project to get update automatically."
fi

# Check if the environment variable $GITLAB_USERNAME is declared else ask for it.
while [ -z "$GITLAB_USERNAME" ]
do
    read -p "What is your GitLab username ? " GITLAB_USERNAME
done

# Check if the environment variable $GITHUB_USERNAME is declared else ask for it.
while [ -z "$GITHUB_USERNAME" ]
do
    read -p "What is your GitHub username ? " GITHUB_USERNAME
done

# Check if the environment variable $GITHUB_TOKEN is declared else ask for it.
while [ -z "$GITHUB_TOKEN" ]
do
    read -p "What is your personal GITHUB_TOKEN ? " GITHUB_TOKEN
done

# Check if the environment variable $GITLAB_TOKEN is declared else ask for it.
while [ -z "$GITLAB_TOKEN" ]
do
    read -p "What is your personal GITLAB_TOKEN ? " GITLAB_TOKEN
done

# Check if the project name to mirror is declared else ask for it.
PROJECT_NAME=$1
while [ -z "$PROJECT_NAME" ]
do
    read -p 'GitHub project to mirror (ex: my-awesome-project): ' PROJECT_NAME
done

echo Mirroring $PROJECT_NAME...

# Get the repository information.
REPO_INFO=$(jq '.[] | select(.path=="'$PROJECT_NAME'")' <<< "$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" https://gitlab.com/api/v4/users/Luc-AUCOIN/projects?search=$PROJECT_NAME&order_by=path)")
# Get ID
REPO_ID=$(jq '.id' <<< "${REPO_INFO}")
# Get privacy
REPO_IS_PRIVATE=$(jq -r  '.visibility' <<< "${REPO_INFO}")

# Cast privacy to boolean
if [ "$REPO_IS_PRIVATE" == "private" ]; then
    REPO_IS_PRIVATE=true
else
    REPO_IS_PRIVATE=false
fi

echo $PROJECT_NAME is being mirrored with:
echo id: $REPO_ID
echo visibility: $REPO_IS_PRIVATE

echo

curl -H "Authorization: token $GITHUB_TOKEN" --data '{"name":"'$PROJECT_NAME'", "private":'$REPO_IS_PRIVATE'}' https://api.github.com/user/repos

echo

# Get topics
REPO_TOPICS=$(jq -r  '.tag_list' <<< "${REPO_INFO}")
REPO_TOPICS=`echo $REPO_TOPICS | sed -e 's/ //g'`
echo Add topics to the new repository: $REPO_TOPICS

echo

curl -X PUT -H "Authorization: token $GITHUB_TOKEN" -H "Accept: application/vnd.github.mercy-preview+json" --data '{"names":'$REPO_TOPICS'}' https://api.github.com/repos/$GITLAB_USERNAME/$PROJECT_NAME/topics

echo

git clone --mirror https://$GITLAB_TOKEN@gitlab.com/$GITLAB_USERNAME/$PROJECT_NAME
cd $PROJECT_NAME.git
git remote rm origin
git remote add origin https://github.com/$GITHUB_USERNAME/$PROJECT_NAME.git
git push origin --all
git push --tags
cd ..
rm -rf $PROJECT_NAME.git

echo

curl --request POST --data "url=https://$GITHUB_USERNAME:$GITHUB_TOKEN@github.com/$GITHUB_USERNAME/$PROJECT_NAME.git&enabled=true" --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/$REPO_ID/remote_mirrors"

echo
echo "Finished !"