# GitLab to GitHub

## This repository

It consists of only one script that will create a GitHub repository which is the mirroring of your GitLab project.
If you push some new feature, fix, test, etc. on GitLab, GitHub will be update automaticly after few minutes.

## How to install and use it ?

1. Clone this repo
2. Execute the following command :
```console
mirror.sh [GITLAB_PROJECT_NAME]
```
OR
```console
mirror.sh
```

## Environment variables

Add these variables in your environment variables
* GITHUB_USERNAME
* [GITHUB_TOKEN](https://github.com/settings/tokens)
* GITLAB_USERNAME
* [GITLAB_TOKEN](https://gitlab.com/profile/personal_access_tokens)

NOTE: If you don't want to add these variables in your environment variables, the script will ask you each time you want to make a mirroring.

## Project path

You can execute the script with **./mirror.sh** or with **./mirror.sh my-awesome-project**\
If you use the first option, the script will ask you th repo name you want to mirror.

## Contributing

* Create an issue and explain your problem
* Fork it / Create your branch / Commit your changes / Push to your branch / Submit a pull request

## Mirror

This repository is available on [GitLab](https://gitlab.com/Luc-AUCOIN/gitlab-to-github--script) and [GitHub](https://github.com/Luc-AUCOIN/gitlab-to-github--script).

## Maintainer

Luc AUCOIN <luc.aucoin1998@gmail.com>